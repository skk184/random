
# create custom Window Setting
plutil -replace 'Window Settings'.Skylar -dictionary ~/Library/Preferences/com.apple.Terminal.plist
# set as default and startup window
plutil -replace 'Window Settings'.Skylar.name -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
plutil -replace 'Startup Window Settings' -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
plutil -replace 'Default Window Settings' -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
# set terminal font
plutil -replace 'Window Settings'.Skylar.Font -data $(cat Font_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
# set terminal text color
plutil -replace 'Window Settings'.Skylar.TextColor -data $(cat TextColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
# set terminal text bold color
plutil -replace 'Window Settings'.Skylar.TextBoldColor -data $(cat TextBoldColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
# set terminal background color
plutil -replace 'Window Settings'.Skylar.BackgroundColor -data $(cat BackgroundColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
