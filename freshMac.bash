#- to get better terminal colours:
#  - background colour: very bottom of colour circle (purpleish) with 3/4 the way to black on slider bar
#  - text: white, all white
#  - font: andale mono 16 pt
#-to set up brew run the following: 
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#-to use the better GNU core utilities: 
brew install coreutils findutils gnu-tar gnu-sed gawk gnutls gnu-indent gnu-getopt
#  - and then add the following two lines to bashrc (to use names without g prefix)
#    PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
#    MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
#-copy all bash files (and vimrc) to home directory from gitlab
#-follow the instructions at the following link to get tab completion activated for git: https://spindance.com/enable-git-tab-completion-bash-mac-os-x/
#-to install google chrome run the following command: brew install google-chrome
#-to install firefox run the following command: 
brew install firefox 
#-to install chromium run the following command: brew install chromium
#-to use basic command line tools run (I believe this is now done automatically when installing brew): xcode-select --install
#-to install more usefull tools run both of the following:
brew install moreutils
#-to install ftp:
brew install inetutils
#  - and then add the following two lines to bashrc
#      PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"
#      MANPATH="/usr/local/opt/inetutils/libexec/gnuman:$MANPATH"
#- turn off edge swipe in chrome (cancer): "defaults write com.google.Chrome.plist AppleEnableSwipeNavigateWithScrolls -bool FALSE"
#- needed for md2pdf
brew install grip
#  - go to https://wkhtmltopdf.org/downloads.html to install wkhtmltopdf

source terminalSettings/terminalSettings.bash

## create custom Window Setting
#plutil -insert 'Window Settings'.Skylar -dictionary ~/Library/Preferences/com.apple.Terminal.plist
## set as default and startup window
#plutil -replace 'Window Settings'.Skylar.name -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
#plutil -replace 'Startup Window Settings' -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
#plutil -replace 'Default Window Settings' -string Skylar ~/Library/Preferences/com.apple.Terminal.plist
## set terminal font
#plutil -replace 'Window Settings'.Skylar.Font -data $(cat terminalSettings/Font_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
## set terminal text color
#plutil -replace 'Window Settings'.Skylar.TextColor -data $(cat terminalSettings/TextColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
## set terminal text bold color
#plutil -replace 'Window Settings'.Skylar.TextBoldColor -data $(cat terminalSettings/TextBoldColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
## set terminal background color
#plutil -replace 'Window Settings'.Skylar.BackgroundColor -data $(cat terminalSettings/BackgroundColor_default.txt) ~/Library/Preferences/com.apple.Terminal.plist
