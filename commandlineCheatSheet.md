# Commandline Cheat Sheet

#### Contents
1. [Unix/Linux Commands](#unixlinuxcommands)
2. [Vim Commands](#vimcommands)

## Unix/Linux Commands <a name="unixlinuxcommands"></a>
##### Contents
1. [Basic Commands](#basiccommands)
2. [Miscellaneous info/tips](#miscellaneousinfotips)

Linux is an open source operating system (OS) based off of the proprietary Unix OS created in 1970s at the Bell Labs research center. For the most part there are very minor differences between the two so most of the following commands will work on both Linux OS's and Unix OS's (Macintosh computers run Unix).

### Basic Commands <a name="basiccommands"></a>

| Command | Description                                                | Usage/Example/Notes            |
|---------|------------------------------------------------------------|--------------------------------|
| ~       | home directory                                             | [click for more info](#~)      |
| .       | current directory                                          | [click for more info](#.)      |
| ..      | previous directory                                         | [click for more info](#..)     |
| ls      | list directory contents                                    | [click for more info](#ls)     |
| pwd     | print working directory                                    | [click for more info](#pwd)    |
| cd      | change directory                                           | [click for more info](#cd)     |
| cp      | copy                                                       | [click for more info](#cp)     |
| mv      | move                                                       | [click for more info](#mv)     |
| mkdir   | make directory                                             | [click for more info](#mkdir)  |
| clear   | clear terminal screen                                      | [click for more info](#clear)  |
| history | displays the commands you entered                          | [click for more info](#history)|
| exit    | closes the terminal                                        | [click for more info](#exit)   |
| diff    | displays the difference between two files                  | [click for more info](#diff)   |
| man     | displays a manual for a command                            | [click for more info](#man)    |
| which   | displays where a command/application is on your computer   | [click for more info](#which)  |
| (xdg-)open | opens a file with a application that can open it        | [click for more info](#open)   |
| vim     | Vim text editor                                            | [click for more info](#vim)    |

### ~ <a name="~"></a>
`~` is an alias for your home directory ie. `/Users/NameOfUser/`

### . <a name="."></a>
`.` is an alias for your current directory.

### .. <a name=".."></a>
`..` is an alias for your previous directory.

### ls <a name="ls"></a>
Usage: `ls <directory path>`. Examples: `ls` without a `<directory path>` will list the files and directories in the current directory. `ls -a` will list **all** files and directories (shows hidden files). `ls -l` will list the files and directories in a long format (displays extra information). Commands such as `-a` and `-l` can be combined ex. `ls -al`. `ls -a ~/Documents` will list all the files and directories in `~/Documents/`.

### pwd <a name="pwd"></a>
Usage: `pwd`. `pwd` will print the full path to the directory you are currently in.

### cd <a name="cd"></a>
Usage: `cd <path to directory>. Examples: `cd ~/Desktop` will take you to Desktop/ `cd ..` will take you to the previous directory. `cd` without any path is the same as calling `cd ~` and will take you to your home directory. `cd -` will take you to the last directory you were in (kinda works like the last channel button on a tv remote).

### cp <a name="cp"></a>
Usage: `cp <name> <location>`. Examples: `cp myFile.txt ../` will copy the file `myFile.txt` to the directory above it. `cp myFile.txt ../../mySecondFile.txt` will copy the file `myFile.txt` and will put it two directories up with the name `mySecondFile.txt`.

### mv <a name="mv"></a>
Usage: `mv <name> <location>`. Examples: `mv myFile.txt ../` will move the file `myFile.txt` to the directory above it. `mv myFile.txt ../../mySecondFile.txt` will move the file `myFile.txt` and will put it two directories up with the name `mySecondFile.txt`. If you want to rename a file you can use `mv` to do so with `mv oldFileName.txt newFileName.txt`, **Note that if `newFileName.txt` already exists it will get overwritten by the `mv` with no warnings or errors unless the `-i` option is used. Using `mv -i fileNameOne.txt fileNameTwo.txt` will warn you if there is already a file named `fileNameTwo.txt` and will require you to enter something that starts with `y` or `Y` in order to continue.**

### mkdir <a name="mkdir"></a>
Usage: `mkdir <new directory>`. Example `mkdir newDirectory` will make a new directory called `newDirectory` in the current directory.

### clear <a name="clear"></a>
Usage: `clear`. `clear` clears everything from the terminal screen (doesn't actually delete anything, you can scroll up to see what you cleared).

### history <a name="history"></a>
Usage: `history`. `history` shows a list of all the commands you have entered since opening the terminal.

### exit <a name="exit"></a>
Usage: `exit`. `exit` will exit the terminal and in some cases close the terminal window as well.

### diff <a name="diff"></a>
Usage: `diff <first file> <second file>`. Examples: `diff roughtDraft.txt finalDraft.txt` will show you all the differences between the file `roughDraft.txt` and the file `finalDraft.txt`. Running this on two identical files will have no output.

### man <a name="man"></a>
Usage: `man <command>`. Example: `man ls` opens a manual for the `ls` command. `man mv` opens a manual for the `mv` command.

### which <a name="which"></a>
Usage: `which <command or application>`. Example: `which python` will tell me where `python` is on my computer. `which ls` will tell me where `ls` is on my computer.

### (xdg-)open <a name="open"></a>
For Linux OS's the command is `xdg-open`. For Unix OS's the command is simply `open`. For the following usage and examples I will use the `open` command but how they work is essentially the same for both OS's.
Usage: `open <application or file>`. Example: `open myFile.pdf` will open `myFile.pdf` with the default application for opening files of that given type (in this case the default application for opening pdf files). A quick google search will show you how to `open` a file or application with the application of your choice. 

### vim <a name="vim"></a>
Usage: `vim <filename>`. Examples: `vim myFile.c` will open the file `myFile.c` if it exists and will create the file and then open it if it doesn't exist.

### Miscellaneous info/tips <a name="miscellaneousinfotips"></a>
1. Most of the time you can press the `tab` key on your keyboard to autocomplete things you are typing into the command line. Pressing `tab` twice in a row will show you all the possible options you have to type next, given the context of what you are typing.

2. Files or directories that start with a `.` are hidden and can only be listed with `ls -a`. Example: if you create a new file called `.hiddenFile.txt` or a new directory called `.hiddenDirectory/` it will appear as though it doesn't exist and can only be seen with `ls -a`.

3. Do not put spaces in file names or directory names as this will cause more problems than it is worth. If you do want to do something with/to a file or directory that has spaces in the name you must use the `\` character to "escape" the space. For example if you wanted to open the file `this file name has spaces.txt` with `vim` you would have to type `vim this\ file\ name\ has\ spaces.txt`. 

4. Many applications and commands will display a short help menu if you use `--help` after the command. Example: `ls --help` displays a helpful information about `ls`. `mv --help` displays helpful information about `mv`.

5. Just like `--help` many applications and commands will display what version of the application or command you have if you run `--version` after it.

6. You can easily make aliases for different commands that you run to make it quicker to run them. For example, if you often work in the directory `~/Documents/myHomework/myAssignment/myQuestion` and in the directory `~/Documents/myOtherHomework/myOtherAssignment/myOtherQuestion` at the same time, and you get tired of using `cd` to move between the two directories you can make an alias to shorten the process. Running `alias myQuestion='cd ~/Documents/myHomework/myAssignment/myQuestion'` and `alias myOtherQuestion='cd ~/Documents/myOtherHomework/myOtherAssignment/myOtherQuestion'` will allow you to simply type `myQuestion` to move to `~/Documents/myHomework/myAssignment/myQuestion` and `myOtherQuestion` to move to `~/Documents/myOtherHomework/myOtherAssignment/myOtherQuestion`. **Note: the aliases will only last until you close the terminal when created this way. Google can help you make permanent aliases.** 

## Vim Commands <a name="vimcommands"></a>
##### Contents
1. [Basic Commands](#basiccommandsvim)

Vim has two main modes `command mode` and what I am going to call `edit mode`. There is no actual `edit mode` but multiple different modes from which you can edit a document in `vim`. The two most important modes for editing is `insert mode` and `visual mode`. `insert mode` is used for adding or removing text just like any basic text editor. `visual mode` is used for basically anything where you would normally use a mouse to highlight text to either copy it or delete it.

### Basic Commands <a name="basiccommandsvim"></a>

| Command | Description                                                | Usage/Example/Notes            |
|---------|------------------------------------------------------------|--------------------------------|
| \<esc\> | exit any mode and go to command mode                       | [click for more info](#esc)    |
| i       | enters insert mode at cursor location                      | [click for more info](#i)      |
| I       | enters insert mode at begining of line                     | [click for more info](#i)      |
| v       | enters visual mode (select by character)                   | [click for more info](#v)      |
| V       | enters visual mode (select by line)                        | [click for more info](#V)      |
| Ctrl-v  | enters visual mode (select by block)                       | [click for more info](#ctrlv)  |
| y       | yank (copy) selection                                      | [click for more info](#y)      |
| yy      | yank (copy) line                                           | [click for more info](#yy)     |
| d       | delete selection                                           | [click for more info](#d)      |
| dd      | delete line                                                | [click for more info](#dd)     |
| p       | paste after cursor (pastes what you yanked)                | [click for more info](#p)      |
| P       | paste before cursor (pastes what you yanked)               | [click for more info](#P)      |
| x       | cuts selection (copies then deletes)                       | [click for more info](#x)      |
| u       | undo last command                                          | [click for more info](#u)      |
| Ctrl-r  | redo (undo your last undo)                                 | [click for more info](#ctrlr)  |
| :w      | write to file (save)                                       | [click for more info](#:w)     |
| :q      | quit file (yells at you if you havent saved)               | [click for more info](#:q)     |
| :q!     | quit file (allows you to quit without saving)              | [click for more info](#:q!)    |
| :vs     | vertically split the screen with another file              | [click for more info](#:vs)    |
| :sv     | horizontally split the screen with another file            | [click for more info](#:sv)    |
| gg      | moves cursor to begining of file                           | [click for more info](#gg)     |
| G       | moves cursor to end of file                                | [click for more info](#G)      |
| /text   | searches for any instance of "text" in the file            | [click for more info](#/)      |

TODO: possibly add more commands

TODO: add "more info" for each command



