#setup nice command prompt; \u:user \h:hostname \w:working directory, everything else is for colours
PS1="\[\033[38;5;40m\]\u\[\033[38;5;15m\]@\[\033[38;5;40m\]\h\[\033[38;5;15m\]:\[\033[38;5;75m\]\w\[\033[38;5;15m\]\$ "

#run .bashrc if it exsists
if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

# add myScripts to path so that I can execute scripts from anywhere
PATH=$PATH:~/gits/random/helperScripts

#needed so that tab completion in git works
source /usr/local/etc/bash_completion.d/git-completion.bash

#needed so that tab completion with make works
complete -W "\`grep -oE '^[a-zA-Z0-9_.-]+:([^=]|$)' Makefile | sed 's/[^a-zA-Z0-9_.-]*$//'\`" make

#needed so that tab completion with homebrew works
if type brew 2&>/dev/null; then
  for completion_file in $(brew --prefix)/etc/bash_completion.d/*; do
    source "$completion_file"
  done
fi

#needed for sphinx
PATH=/usr/local/opt/sphinx-doc/bin:$PATH
#needed for latex
PATH=/Library/TeX/texbin:$PATH
