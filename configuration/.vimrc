"Numbers editor lines
set number
"Add filename to bottom of screen
set ls=2
"Favourite colour scheme, others being: elflord, torte, slate, industry
colorscheme pablo
"Turns on syntax highlighting
syntax on
"The width of a TAB is set to 2
"Still it is a \t. It is just that 
"Vim will interpret it to be having 
"a width of 2.
set tabstop=2
"Indents will have a width of 2
set shiftwidth=2
"Sets the number of columns for a TAB
set softtabstop=2
"Expand TABs to space
set expandtab
"Automatically indents on return
set autoindent
"Line breaks will break on breakat option (space is a default)
set linebreak
"Allows you to use plugins
filetype plugin on
"Hard Tabs in makefiles only
autocmd FileType make setlocal noexpandtab
"If you need a hard tab (outside makefiles)
"remapped:Shift+Tab (default is CTRL+V escape then tab from insert)
inoremap <S-Tab> <C-V><Tab>
"Autocompletes parenthesis
inoremap {      {}<Left>
inoremap (      ()<Left>
inoremap [      []<Left>
inoremap {<CR>  {<CR>}<Esc>O<Tab>
inoremap (<CR>  (<CR>)<Esc>O<Tab>
inoremap [<CR>  [<CR>]<Esc>O<Tab>
inoremap {{     {
inoremap ((     (
inoremap [[     [
"Autocompletes quotes
inoremap "  ""<Left>
inoremap '  ''<Left>
inoremap "" "
inoremap '' '
set pastetoggle=<F2>
"Maps jk and kj  to Esc
inoremap jk <Esc>
inoremap kj <Esc>ll
vnoremap jk <Esc>
vnoremap kj <Esc>ll
"Maps df and fd to :wq
noremap df :wq<CR>
noremap fd :wq<CR>
"Essentially turns off all aliases for clean pasting
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
"Turn on spell checker. Sets syntax based on filetype extension
"%=currentfilename, :e=modifies % to show only extension
nnoremap <F3> :set spell spelllang=en_ca<CR> :set syntax="%:e"<CR>
"Setup incrimental search, erase highlight by pressing spacebar
set hlsearch
set incsearch
nnoremap <silent> <space> :noh<cr><esc>


"Plugin info
"###########
"Plugin for real time markdown rendering in browser:
"https://github.com/suan/vim-instant-markdown
