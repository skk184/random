#general aliases
alias cl='clear'
alias o='open'
alias ls='ls -F --color=auto'

#git aliases
alias gs='git status'

#web aliases
alias google='o https://www.google.ca'
alias netflix='o https://www.netflix.com'
alias crunchyroll='o https://www.crunchyroll.com'
alias gitlab='o https://www.gitlab.com'
alias github='o https://www.github.com'
alias paws='o https://paws5.usask.ca'

alias Bulbasaur=open /Users/skylar/Documents/Bulbasaur.png
